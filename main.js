document.addEventListener("DOMContentLoaded", (event) => {
  const itemForm = document.getElementById("itemForm");
  const urlInput = document.getElementById("url");
  const nameInput = document.getElementById("name");
  const priceInput = document.getElementById("price");
  const cartItems = document.getElementById("cartItems");
  const totalDiv = document.getElementById("total");

  let items = JSON.parse(localStorage.getItem("cartItems")) || [];
  items.forEach((item) => {
    if (item.favorite === undefined) {
      item.favorite = false;
    }
  });

  function updateCart() {
    cartItems.innerHTML = "";
    let total = 0;
    items.forEach((item, index) => {
      const itemDiv = document.createElement("div");
      itemDiv.innerHTML = `
                <div class="flex justify-between items-center mb-2">
                    <a href="${item.url}" target="_blank" class="text-blue-500">${item.name}</a>
                    <span>$${item.price}</span>
                    <button onclick="removeItem(${index})" class="text-red-500"><i class="fas fa-trash"></i></button>
                </div>
            `;
      cartItems.appendChild(itemDiv);
      total += Number(item.price);
    });
    totalDiv.textContent = `Total: $${total.toFixed(2)}`;
    localStorage.setItem("cartItems", JSON.stringify(items));
  }

  function removeItem(index) {
    items.splice(index, 1);
    updateCart();
  }

  window.removeItem = removeItem;

  itemForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const url = urlInput.value;
    const name = nameInput.value;
    const price = priceInput.value;
    if (url && name && price) {
      items.push({ url, name, price });
      urlInput.value = "";
      nameInput.value = "";
      priceInput.value = "";
      updateCart();
    }
  });

  updateCart();
});
